# Base VueJS (Vue3)

## Local run
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Lints and fixes files
```
yarn lint
```

## Docker run

### Docker setup and hot-reloads for development
```docker-compose up -d```

## Library

- Axios
- Bootstrap & Bootstrap Vue
- Font Awesome
- Element UI
- JWT decode
- Vee validate
- Vue i18n
- Vuex